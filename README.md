Q1. Does the function run correctly? If not then why?
    
    function isObject(a) {
    	If (typeof a === “object”) {
    		return true;
    } else {
    	return false;
        }
    }
    
Q2. What's the output of the function. Give reason for the same.

    function adds() {
        var a  = 0.2 + 0.4;
        if (a == 0.6) {
            alert('Yes');
        } else {
            alert('No');
        }
    }


Q3. Observe the code in index.html and example.js files. The code has some error.
    Find out where and why this error occurs.
    
Q4. Observe the code in test-jquery.html. Find out why the alert box does not pop up.